﻿' Project:P151A_Train1
' Auther:IE2A No.24, 村田直人
' Date: 2015年05月01日



Public Class Form1

    '配列の要素数を宣言
    Const Last As Integer = 9

    Dim LabeIntTop As Integer 'ラベルのY座標の初期値を格納する変数
    Dim CopyDistance As Integer 'ラベルの配列間隔を格納する変数
    Dim FormInitSize As Size 'フォームのサイズ（初期値）を格納しておく変数

    'ボタンのテキスト変数
    Const Startstr As String = "スタート"
    Const Resetstr As String = "リセット"


    Dim TrainLabels(Last) As Label          'ラベル配列の要素数をLastを使って宣言
    Dim TrainRandom As Random = New Random  'Randomクラスのインスタンス化

    'フォームが呼び出された時の処理
    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles Me.Load


        LabeIntTop = TrainLabel.Location.Y          'ラベルY座標を変数に格納
        CopyDistance = TrainLabel.Size.Width + 12   'ラベルの配置間隔変数にラベルサイズ＋12を格納

        'フォームプロパティの設定
        With Me
            .Size = New Size(.Size.Width + CopyDistance * Last, .Size.Height) 'フォームサイズの横幅を設定する
            .MinimumSize = New Size(.Size.Width, 200)       'フォームの最少サイズを「200」に設定
            .MaximumSize = New Size(.Size.Width, 400)       'フォーム(縦)の最大サイズを「400」に設定
        End With

        TrainLabels(0) = TrainLabel 'ラベルの参照をTrainLabelsの先頭の要素に代入

        '配列にラベルを格納してプロパティを設定する処理
        For i As Integer = 1 To Last

            TrainLabels(i) = New Label 'ラベルのインスタンス化して配列に格納

            'ラベルのプロパティ設定
            With TrainLabels(i)
                .Image = TrainLabel.Image                'イメージ設定
                .Size = TrainLabel.Size                  'サイズ設定
                .Location = New Point(TrainLabels(i - 1).Location.X + CopyDistance, LabeIntTop) 'ロケーション設定
                .ImageAlign = ContentAlignment.TopCenter 'ラベルの位置を設定
            End With
        Next

        Me.Controls.AddRange(TrainLabels)   'フォームにラベルを追加

        StartButton.Text = Startstr         'ボタンのテキストに「スタート」を表示

    End Sub

    'スタート、リセットボタン
    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles StartButton.Click

        'onClick時の処理分岐
        If StartButton.Text = Startstr Then

            StartButton.Text = Resetstr 'ボタンのテキスト変更
            Timer1.Start()              'タイマー起動

        Else

            StartButton.Text = Startstr 'ボタンのテキストを変更
            Timer1.Stop()               'タイマーを停止

            'ラベルのY座標を初期化する処理
            For Each tLabel As Label In TrainLabels
                tLabel.Location = New Point(tLabel.Location.X, LabeIntTop) 'ロケーションを設定
            Next

            Me.Size = Me.MinimumSize 'フォームのサイズを最少に戻す

        End If

    End Sub

    'タイマー処理
    Private Sub Timer1_Tick(sender As Object, e As EventArgs) Handles Timer1.Tick

        Dim n As Integer = TrainRandom.Next(Last + 1) 'ランダムな数字を格納

        'ランダムにラベルを下げる
        With TrainLabels(n)
            .Location = New Point(.Location.X, .Location.Y + 5) 'ロケーションを設定
        End With

    End Sub
End Class
